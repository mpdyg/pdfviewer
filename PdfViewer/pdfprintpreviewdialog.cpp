/***************************************************************************
**
** Copyright (C) 2018 Eduardo Adrian González <mecprecdyg@gmail.com>
**
** This file is part of the PdfViewer library.
**
** The PdfViewer is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** The PdfViewer is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with the PdfViewer.  If not, see <http://www.gnu.org/licenses/>.
**
***************************************************************************/


#include "pdfprintpreviewdialog.h"
#include "pdfprintpreviewdialogactionfilter.h"

#include <QToolBar>

#include <QDebug>
#include <QEvent>
#include <QPushButton>
#include <QPrinter>
#include <QLineEdit>

PdfPrintPreviewDialog::PdfPrintPreviewDialog(QPrinter *printer, QWidget *parent, Qt::WindowFlags flags)
    : QPrintPreviewDialog(printer, parent, flags)
{
    printPressed = false;
    setWindowTitle(tr("Preview"));
    QToolBar *toolBar = findChild<QToolBar*>();
    pageNumberEdit = findChild<QLineEdit*>();
    QList<QAction*> lst = findChildren<QAction*>();
    for(QAction *a: lst){
        if(a->text() == QStringLiteral("Print")){
            QWidget *w = toolBar->widgetForAction(a);
            w->installEventFilter(new PdfPrintPreviewDialogActionFilter(this));
        }
    }
}

bool PdfPrintPreviewDialog::isPrintPressed() const
{
    return printPressed;
}

int PdfPrintPreviewDialog::currentPage()
{
    if(pageNumberEdit){
        return pageNumberEdit->text().toInt();
    }
    return 0;
}

