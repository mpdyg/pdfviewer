#/***************************************************************************
#**
#** Copyright (C) 2018 Eduardo Adrian González <mecprecdyg@gmail.com>
#**
#** This file is part of the PdfViewer library.
#**
#** The PdfViewer is free software: you can redistribute it and/or modify
#** it under the terms of the GNU General Public License as published by
#** the Free Software Foundation, either version 3 of the License, or
#** (at your option) any later version.
#**
#** The PdfViewer is distributed in the hope that it will be useful,
#** but WITHOUT ANY WARRANTY; without even the implied warranty of
#** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#** GNU General Public License for more details.
#**
#** You should have received a copy of the GNU General Public License
#** along with the PdfViewer.  If not, see <http://www.gnu.org/licenses/>.
#**
#***************************************************************************/


#-------------------------------------------------
#
# Project created by QtCreator 2018-10-23T10:31:58
#
#-------------------------------------------------

QT       += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

TARGET = PdfViewer
TEMPLATE = lib

DEFINES += PDFVIEWER_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        pdfviewer.cpp \
    pdfprintpreviewdialog.cpp \
    pdfprintpreviewdialogactionfilter.cpp

HEADERS += \
        pdfviewer.h \
        pdfviewer_global.h \ 
    pdfprintpreviewdialog.h \
    pdfprintpreviewdialogactionfilter.h



unix:!macx: LIBS += -lpoppler-qt5

INCLUDEPATH += \
/usr/include/poppler/qt5/poppler-annotation.h \
/usr/include/poppler/qt5/poppler-export.h \
/usr/include/poppler/qt5/poppler-form.h \
/usr/include/poppler/qt5/poppler-link.h \
/usr/include/poppler/qt5/poppler-media.h \
/usr/include/poppler/qt5/poppler-optcontent.h \
/usr/include/poppler/qt5/poppler-page-transition.h \
/usr/include/poppler/qt5/poppler-qt5.h
DEPENDPATH += \
/usr/include/poppler/qt5/poppler-annotation.h \
/usr/include/poppler/qt5/poppler-export.h \
/usr/include/poppler/qt5/poppler-form.h \
/usr/include/poppler/qt5/poppler-link.h \
/usr/include/poppler/qt5/poppler-media.h \
/usr/include/poppler/qt5/poppler-optcontent.h \
/usr/include/poppler/qt5/poppler-page-transition.h \
/usr/include/poppler/qt5/poppler-qt5.h
