/***************************************************************************
**
** Copyright (C) 2018 Eduardo Adrian González <mecprecdyg@gmail.com>
**
** This file is part of the PdfViewer library.
**
** The PdfViewer is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** The PdfViewer is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with the PdfViewer.  If not, see <http://www.gnu.org/licenses/>.
**
***************************************************************************/


#ifndef PDFVIEWER_H
#define PDFVIEWER_H

#include "pdfviewer_global.h"

#include <QObject>
#include <QPrinter>
#include <QWidget>

namespace Poppler {
    class Document;
}

class PdfPrintPreviewDialog;

class PDFVIEWERSHARED_EXPORT  PdfViewer : public QObject
{
    Q_OBJECT
public:
    explicit PdfViewer(QWidget *parent = nullptr);
    void loadPdf(QString path);

signals:

public slots:

private:
    QPrinter *printer;
    Poppler::Document* document;
    QWidget *parent;
    PdfPrintPreviewDialog *dialog;

    void printDocument(QPrinter *printer);
};

#endif // PDFVIEWER_H
