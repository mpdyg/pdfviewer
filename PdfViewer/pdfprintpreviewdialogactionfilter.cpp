/***************************************************************************
**
** Copyright (C) 2018 Eduardo Adrian González <mecprecdyg@gmail.com>
**
** This file is part of the PdfViewer library.
**
** The PdfViewer is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** The PdfViewer is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with the PdfViewer.  If not, see <http://www.gnu.org/licenses/>.
**
***************************************************************************/


#include "pdfprintpreviewdialog.h"
#include "pdfprintpreviewdialogactionfilter.h"

#include <QEvent>
#include <QPrinter>

PdfPrintPreviewDialogActionFilter::PdfPrintPreviewDialogActionFilter(PdfPrintPreviewDialog *parent)
    : QObject(parent)
    , dlg(parent)
{

}

bool PdfPrintPreviewDialogActionFilter::eventFilter(QObject *dist, QEvent *event)
{
    Q_UNUSED(dist);
    if( event->type() == QEvent::MouseButtonPress )
    {
        dlg->printPressed = true;
        dlg->printer()->setFromTo(1, dlg->numPages);
    }
    return false;
}
