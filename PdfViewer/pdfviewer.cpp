/***************************************************************************
**
** Copyright (C) 2018 Eduardo Adrian González <mecprecdyg@gmail.com>
**
** This file is part of the PdfViewer library.
**
** The PdfViewer is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** The PdfViewer is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with the PdfViewer.  If not, see <http://www.gnu.org/licenses/>.
**
***************************************************************************/


#include "pdfprintpreviewdialog.h"
#include "pdfviewer.h"

#include <QFile>
#include <QMessageBox>
#include <QPainter>
#include <QPrinter>
#include <poppler/qt5/poppler-qt5.h>
#include <QPrintPreviewDialog>

#include <QDebug>

#define IMAGE_DPI 150.0

PdfViewer::PdfViewer(QWidget *parent)
    : QObject(parent)
    , printer(new QPrinter())
    , document(nullptr)
    , dialog(nullptr)
{

}

void PdfViewer::loadPdf(QString path)
{
    if(document){
        delete document;
        document = nullptr;
    }
    if(!path.isEmpty()){
        document = Poppler::Document::load(path);
        if (!document || document->isLocked()) {
            QMessageBox::critical(parent, tr("Error"), tr("The pdf archive can't be loaded."));
            delete document;
            document = nullptr;
            return;
        }
        PdfPrintPreviewDialog dlg;
        dialog = &dlg;
        dlg.numPages = document->numPages();
        connect(&dlg, &QPrintPreviewDialog::paintRequested, this, &PdfViewer::printDocument);
        dlg.exec();
        disconnect(&dlg, &QPrintPreviewDialog::paintRequested, this, &PdfViewer::printDocument);
        dialog = nullptr;
    }
}

void PdfViewer::printDocument(QPrinter *printer)
{
    QPainter painter;
    if(!painter.begin(printer)) {
        QMessageBox::critical(parent, tr("Error"), tr("The printer can't be reached."));
        return;
    }

    // Paranoid safety check
    if (document == nullptr) {
        QMessageBox::critical(parent, tr("Error"), tr("The pdf archive can't be loaded."));
      return;
    }
    int collateCount = printer->collateCopies() ? printer->copyCount() : 1;
    int simpleCount = printer->collateCopies() ? 1 : printer->copyCount();
    int from = 0;
    int to = 0;
    if(dialog && !dialog->isPrintPressed()){
        from = 1;
        to = document->numPages();
        collateCount = 1;
        simpleCount = 1;
    }
    else{
        switch (printer->printRange()) {
        case QPrinter::AllPages:
            from = 1;
            to = document->numPages();
            break;
        case QPrinter::Selection:
            // can't be reached
            from = 1;
            to = document->numPages();
            break;
        case QPrinter::PageRange:
            from = printer->fromPage();
            to = printer->toPage();
            break;
        case QPrinter::CurrentPage:
            if(dialog){
                from = dialog->currentPage();
                to = dialog->currentPage();
            }
            else{
                from = 0;
                to = 0;
            }
            break;
        }
        if(printer->pageOrder() == QPrinter::LastPageFirst){
            int a = to;
            to = from;
            from = a;
        }
    }
    for(int i=0; i<collateCount; i++){
        for(int pageNumber = from-1; pageNumber<to && pageNumber<document->numPages();
            printer->pageOrder() == QPrinter::LastPageFirst ? pageNumber-- : pageNumber++){
            // Access page of the PDF file
            Poppler::Page* pdfPage = document->page(pageNumber);  // Document starts at page 0
            if (pdfPage == nullptr) {
                QMessageBox::critical(parent, tr("Error"), QString(tr("The page number %1 can't be loaded.")).arg(pageNumber));
                return;
            }
            // Generate a QImage of the rendered page
            QImage image = pdfPage->renderToImage(IMAGE_DPI,IMAGE_DPI);
            if (image.isNull()) {
                QMessageBox::critical(parent, tr("Error"), QString(tr("The page number %1 can't be rendered.")).arg(pageNumber));
                return;
            }

            QRect r(0,0,image.width(),image.height());

            if(pageNumber != 0)
                printer->newPage();
            for(int i=0; i<simpleCount; i++){
                if(i > 0)
                    printer->newPage();
                painter.drawImage(painter.window(), image, r/*, pdfPage->pageSize()*/);
            }
            // after the usage, the page must be deleted
            delete pdfPage;
        }
    }
}
