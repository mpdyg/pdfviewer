/***************************************************************************
**
** Copyright (C) 2018 Eduardo Adrian González <mecprecdyg@gmail.com>
**
** This file is part of the PdfViewer library.
**
** The PdfViewer is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** The PdfViewer is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with the PdfViewer.  If not, see <http://www.gnu.org/licenses/>.
**
***************************************************************************/


#ifndef PDFPRINTPREVIEWDIALOG_H
#define PDFPRINTPREVIEWDIALOG_H

#include "pdfviewer_global.h"

#include <QPrintPreviewDialog>

class ActionFilter;
class QLineEdit;

class PDFVIEWERSHARED_EXPORT PdfPrintPreviewDialog : public QPrintPreviewDialog
{

public:
    PdfPrintPreviewDialog(QPrinter *printer = nullptr, QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

    bool isPrintPressed() const;
    int currentPage();

private:
    QLineEdit *pageNumberEdit;
    bool printPressed;
    char dummy[3];
public:
    int numPages = 0;

    friend class PdfPrintPreviewDialogActionFilter;
};

#endif // PDFPRINTPREVIEWDIALOG_H
