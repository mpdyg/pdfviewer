 
This project was born out of the need of a simple pdf viewer.
It's been developed using qt-5.11.
It uses Poppler library to render pdf documemnts.
You would need to install libpoppler-qt5-dev in your system in order to run the project.
This project was develop for linux. I WILL NOT DO ANY CHANGES FOR WINDOWS.
The resolution can be changed by changing the defined constant IMAGE_DPI in the file "pdfviewer.cpp", at this time is set at 150 DPI, as I needed only to show simple pdf files.
Beware that setting this value too high the rendering process would take longer.
It take couple of seconds to load a 200 pages pdf file at this resolution.

There is a subproject inside this project that shows how to use this library.
I don't expect to develop this project any further.
Any improvements will be very welcome. Feel free to fork this project.
I don't have much experience with online git repos, so If you'd like to push an improvement I would gladly do my best to acomplish it.

I published this project to thanks all people that published open source software, that help me to develop my programing skills.
It is a modest gesture to add to the open source community. Thank you very much to all of you.
